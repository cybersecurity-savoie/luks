#!/bin/bash

#Variables
PROGNAME=$0
mount_directory="/mnt/encrypted"
encrypted_image="enc.iso"

#Functions
usage() {
  cat << EOF >&2
Usage: $PROGNAME [-ocn]

-o: open and mount the volume
-c: umount and close the volume
-n: create a new partition

EOF
  exit 1
}

open_enc(){
    sudo cryptsetup open $encrypted_image encVolume
    sudo mount /dev/mapper/encVolume $mount_directory
}

close_enc(){
    sudo umount /dev/mapper/encVolume
    sudo cryptsetup close encVolume
}

create_enc(){
    echo "create a new encrypted partition"
}
if [[ ${#} -eq 0 ]]; then
   usage
fi

#Menu
while getopts :ocn flag
do
    case "${flag}" in
        o) open_enc;;
        c) close_enc;;
        n) create_enc;;
        *) usage;;
        :) echo "$0: Must supply an argument to -$OPTARG." >&2
           exit 1
           ;;
        ?) echo "Invalid option: -${OPTARG}."
            exit 2
            ;;
    esac
done